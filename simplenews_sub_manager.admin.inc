<?php
/**
 * @file
 * The admin pages to manage and add emails address to simplenews for managers.
 */

/**
 * Mass import email address for the newsletters a manager has access to.
 *
 * @return array
 *  For for importing email address to Simplenews.
 */
function simplenews_sub_manager_import() {
  global $language, $user;

  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline.'),
  );
  $newsletters = array();
  foreach (taxonomy_get_tree(variable_get('simplenews_vid', '')) as $newsletter) {
    $newsletters[$newsletter->tid] = check_plain($newsletter->name);
  }

  $my_newsletters = array();
  $result = db_query("SELECT tid FROM {simplenews_sub_manager} WHERE uid = %d", $user->uid);
  while ($item = db_fetch_object($result)) {
    $my_newsletters[$item->tid] = NULL;
  }

  $form['newsletters'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Subscribe to'),
    '#options' => array_intersect_key($newsletters, $my_newsletters),
    '#required' => TRUE,
  );

  // Include language selection when the site is multilingual.
  // Default value is the empty string which will result in receiving emails
  // in the site's default language.
  if (variable_get('language_count', 1) > 1) {
    $options[''] = t('Site default language');
    $languages = language_list('enabled');
    foreach ($languages[1] as $langcode => $item) {
      $name = t($item->name);
      $options[$langcode] = $name . ($item->native != $name ? ' ('. $item->native .')' : '');
    }
    $form['language'] = array(
      '#type' => 'radios',
      '#title' => t('Anonymous user preferred language'),
      '#default_value' => '',
      '#options' => $options,
      '#description' => t('Anonymous users will be subscribed with the selected preferred language. They will receive newsletters in this language if available. Registered users will be subscribed with their preferred language as set on their account page.'),
    );
  }
  else {
    $form['language'] = array('#type' => 'value', '#value' => '');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  return $form;
}

function simplenews_sub_manager_import_submit($form, &$form_state) {
  $tree = taxonomy_get_tree(variable_get('simplenews_vid', ''));
  $added = array();
  $invalid = array();
  $checked_newsletters = array_filter($form_state['values']['newsletters']);
  $langcode = $form_state['values']['language'];

  $emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  foreach ($emails as $email) {
    $email = trim($email);
    if (valid_email_address($email)) {
      foreach ($checked_newsletters as $tid) {
        $newsletter = taxonomy_get_term($tid);
        simplenews_subscribe_user($email, $newsletter->tid, FALSE, $langcode);
        $added[] = $email;
      }
    }
    else {
      $invalid[] = $email;
    }
  }
  if ($added) {
    $added = implode(", ", $added);
    drupal_set_message(t('The following addresses were added or updated: %added.', array('%added' => $added)));

    $newsletter_names = array();
    foreach ($checked_newsletters as $tid) {
      $newsletter = taxonomy_get_term($tid);
      $newsletter_names[] = $newsletter->name;
    }
    drupal_set_message(t('The addresses were subscribed to the following newsletters: %newsletters.', array('%newsletters' => implode(', ', $newsletter_names))));
  }
  else {
    drupal_set_message(t('No addresses were added.'));
  }
  if ($invalid) {
    $invalid = implode(", ", $invalid);
    drupal_set_message(t('The following addresses were invalid: %invalid.', array('%invalid' => $invalid)), 'error');
  }
}

function simplenews_sub_manager_page($form_state) {
  // Delete subscriptions requires delete confirmation. This is handled with a different form
  if (isset($form_state['post']['operation']) && $form_state['post']['operation'] == 'delete' && isset($form_state['post']['snids'])) {
    $destination = drupal_get_destination();
    $_SESSION['simplenews_sub_manager_subscriptions_delete'] = $form_state['post']['snids'];
    // Note: we redirect from admin/content/simplenews/users to admin/content/simplenews/subscriptions/delete to make the tabs disappear.
    drupal_goto("simplenews_sub_manager/delete", $destination);
  }

  $form = simplenews_sub_manager_page_form();
  $form['admin'] = simplenews_sub_manager_subscription_list_form();

  return $form;
}

/**
 * Return form for subscription filters.
 *
 * @see simplenews_sub_manager_subscription_filter_form_submit()
 */
function simplenews_sub_manager_page_form() {
  // Current filter selections in $session var; stored at form submission
  // Example: array('newsletter' => 'all', 'email' => 'hotmail')
  $form = array();
  $session = isset($_SESSION['simplenews_sub_manager_subscriptions_filter']) ? $_SESSION['simplenews_sub_manager_subscriptions_filter'] : NULL;
  $session = is_array($session) ? $session : _simplenews_sub_manager_page_default();
  $filters = simplenews_sub_manager_subscription_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription filters'),
    '#collapsible' => FALSE,
    '#prefix' => '<div class="simplenews-subscription-filter">',
    '#suffix' => '</div>',
  );

  // Filter values are default
  $form['filters']['newsletter'] = array(
    '#type' => 'select',
    '#title' => $filters['newsletter']['title'],
    '#options' => $filters['newsletter']['options'],
    '#default_value' => $session['newsletter'],
  );
  $form['filters']['email'] = array(
    '#type' => 'textfield',
    '#title' => $filters['email']['title'],
    '#default_value' => $session['email'],
  );
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#prefix' => '<span class="spacer" />',
  );
  // Add Reset button if filter is in use
  if ($session != _simplenews_sub_manager_page_default()) {
    $form['filters']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }
  $form['#submit'][] = 'simplenews_sub_manager_subscription_filter_form_submit';

  return $form;
}

/**
 * Helper function: returns subscription filter default settings
 */
function _simplenews_sub_manager_page_default() {
  return array('newsletter' => 'all', 'email' => '');
}

function simplenews_sub_manager_subscription_filter_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Filter'):
      $_SESSION['simplenews_sub_manager_subscriptions_filter'] = array(
        'newsletter' => $form_state['values']['newsletter'],
        'email' => $form_state['values']['email'],
      );
      break;
    case t('Reset'):
      $_SESSION['simplenews_sub_manager_subscriptions_filter'] = _simplenews_sub_manager_page_default();
      break;
  }
}

/**
 * Generate subscription filters
 */
function simplenews_sub_manager_subscription_filters() {
  // Newsletter filter
  $filters['newsletter'] = array(
    'title' => t('Subscribed to'),
    'options' => array(
      'all'   => t('All newsletters'),
    ),
  );

  // Get newsletters a user has access to.
  global $user;
  $my_newsletters = array();
  $result = db_query("SELECT tid FROM {simplenews_sub_manager} WHERE uid = %d", $user->uid);
  while ($item = db_fetch_object($result)) {
    $my_newsletters[$item->tid] = NULL;
  }
  
  // Get all newsletters.
  $newsletters = array();
  foreach (taxonomy_get_tree(variable_get('simplenews_vid', '')) as $newsletter) {
    $newsletters[$newsletter->tid] = array(
      'tid' => $newsletter->tid,
      'name' => $newsletter->name,
    );
  }
  
  // Reduce list to only ones a user has access to.
  $newsletters = array_intersect_key($newsletters, $my_newsletters);
  
  foreach ($newsletters as $newsletter) {
    $filters['newsletter']['options']['tid-'. $newsletter['tid']] = $newsletter['name'];
  }

  // Email filter
  $filters['email'] = array(
    'title' => t('Email address'),
  );

  return $filters;
}

/**
 * Build the form for admin subscription.
 *
 * Form consists of a filter fieldset, an operation fieldset and a list of
 * subscriptions maching the filter criteria.
 *
 * @see simplenews_sub_manager_subscription_list_form_validate()
 * @see simplenews_sub_manager_subscription_list_form_submit()
 */
function simplenews_sub_manager_subscription_list_form() {
  // Table header. Used as tablesort default
  $header = array(
    array('data' => t('Email'), 'field' => 'ss.mail', 'sort' => 'asc'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'ss.activated'),
    t('Operations')
  );

  // Data collection with filter and sorting applied
  $filter = simplenews_sub_manager_build_subscription_filter_query();
  $query = 'SELECT DISTINCT ss.*, u.name FROM {simplenews_subscriptions} ss INNER JOIN {users} u ON ss.uid = u.uid INNER JOIN {simplenews_snid_tid} s ON ss.snid = s.snid'. $filter['where'];

  // $count_query used to count distinct records only
  $count_query = preg_replace('/SELECT.*FROM /', 'SELECT COUNT(DISTINCT ss.mail) FROM ', $query);
  $query .= tablesort_sql($header);
  $result = pager_query($query, 30, 0, $count_query);

  // Subscription table and table pager
  while ($subscription = db_fetch_object($result)) {
    $snids[$subscription->snid] = '';
    $form['mail'][$subscription->snid] = array('#value' => $subscription->mail);
    $form['name'][$subscription->snid] =  array('#value' => isset($subscription->uid) ? l($subscription->name, 'user/'. $subscription->uid) : $subscription->name);
    $form['status'][$subscription->snid] = array('#value' => theme('simplenews_status', $subscription->activated, 'activated'));
    $form['operations'][$subscription->snid] = array('#value' => l(t('edit'), 'simplenews_sub_manager/edit/'. $subscription->snid, array(), drupal_get_destination()));
  }
  $form['pager'] = array('#value' => theme('pager', NULL, 30, 0));
  $form['#theme'] = 'simplenews_sub_manager_subscription_list';
  return $form;
}

/**
 * Build query for subscription filters based on session var content.
 *
 * @return array of SQL query parts
 *   array('where' => $where, 'join' => $join, 'args' => $args)
 */
function simplenews_sub_manager_build_subscription_filter_query() {
  // Variables $args and $join are currently not used but left in for future extensions
  $where = $args = array();
  $join = '';

  // Build query
  if (isset($_SESSION['simplenews_sub_manager_subscriptions_filter'])) {
    foreach ($_SESSION['simplenews_sub_manager_subscriptions_filter'] as $key => $value) {
    switch ($key) {
        case 'newsletter':
          if ($value != 'all') {
            list($key, $value) = explode('-', $value, 2);
            $where[] = 's.'. $key .' = '. $value;
          }
          break;
        case 'email':
          if (!empty($value)) {
            $where[] = "ss.mail LIKE '%%". db_escape_string($value) ."%%'";
          }
          break;
      }
      $args[] = $value;
    }
  }

  // Make sure only the tids the user has access to are used.
  global $user;
  $my_newsletters = array();
  $result = db_query("SELECT tid FROM {simplenews_sub_manager} WHERE uid = %d", $user->uid);
  while ($item = db_fetch_object($result)) {
    $my_newsletters[] = $item->tid;
  }
  $where[] = 's.tid IN ('. implode(', ', $my_newsletters) .')';

  // All conditions are combined with AND
  $where = empty($where) ? '' : ' AND '. implode(' AND ', $where);

  return array('where' => $where, 'join' => $join, 'args' => $args);
}

function theme_simplenews_sub_manager_subscription_list($form) {
  // Subscription table header
  $header = array(
    array('data' => t('Email'), 'field' => 'ss.mail', 'sort' => 'asc'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'ss.activated'),
    t('Operations')
  );

  // Subscription table
  $output = drupal_render($form['options']);
  if (isset($form['mail']) && is_array($form['mail'])) {
    foreach (element_children($form['mail']) as $key) {
      $row = array();
      $row[] = drupal_render($form['mail'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['status'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }
  }
  else  {
    $rows[] = array(array('data' => t('No subscriptions available.'), 'colspan' => '4'));
  }

  // Render table header, pager and form
  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);

  return $output;
}

/**
 * Menu callback: handle the edit subscription page and a subscription
 * page for anonymous users.
 */
function simplenews_sub_manager_users_form(&$form_state, $snid = NULL) {
  $form = simplenews_sub_manager_subscription_manager_form($form_state, $snid);
  $form['#redirect'] = 'simplenews_sub_manager';
  return $form;
}

/**
 * Menu callback: Generates the subscription form for users.
 *
 * @see simplenews_subscription_manager_form_validate()
 * @see simplenews_subscription_manager_form_submit()
 */
function simplenews_sub_manager_subscription_manager_form(&$form_state, $snid = NULL) {

  $account = new stdClass();
  $account->snid = $snid;
  $subscription = simplenews_get_subscription($account);
  $form = _simplenews_sub_manager_subscription_manager_form($subscription);
  $form['#validate'][] = 'simplenews_sub_manager_subscription_manager_form_validate';
  $form['#submit'][] = 'simplenews_sub_manager_subscription_manager_form_submit';
  $form['#redirect'] = '';  //Return to home page after (un)subscribe

  return $form;
}

function simplenews_sub_manager_subscription_manager_form_validate($form, &$form_state) {
  $valid_email = valid_email_address($form_state['values']['mail']);
  if (!$valid_email) {
    form_set_error('mail', t('The email address you supplied is not valid.'));
  }
  $checked_newsletters = array_filter($form_state['values']['newsletters']);
  $account = new stdClass();
  $account->mail = $form_state['values']['mail'];
  if (!count($checked_newsletters) && !simplenews_get_subscription($account)) {
    form_set_error('newsletters', t('You must select at least one newsletter.'));
  }
}

function simplenews_sub_manager_subscription_manager_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Update'):
      foreach ($form_state['values']['newsletters'] as $tid => $checked) {
        if ($checked) {
          simplenews_subscribe_user($form_state['values']['mail'], $tid, FALSE);
        }
        else {
          simplenews_unsubscribe_user($form_state['values']['mail'], $tid, FALSE);
        }
      }
      drupal_set_message(t('The newsletter subscriptions for %mail have been updated.', array('%mail' => $form_state['values']['mail'])));
      break;
    case t('Subscribe'):
      foreach ($form_state['values']['newsletters'] as $tid => $checked) {
        if ($checked) {
          simplenews_subscribe_user($form_state['values']['mail'], $tid);
        }
      }
      drupal_set_message(t('You will receive a confirmation email shortly containing further instructions on how to complete your subscription.'));
      break;
    case t('Unsubscribe'):
      foreach ($form_state['values']['newsletters'] as $tid => $checked) {
        if ($checked) {
          simplenews_unsubscribe_user($form_state['values']['mail'], $tid);
        }
      }
      drupal_set_message(t('You will receive a confirmation email shortly containing further instructions on how to complete the unsubscription process.'));
      break;
  }
}

/**
 * Build subscription manager form.
 *
 * @param object $subscription subscription object
 */
function _simplenews_sub_manager_subscription_manager_form($subscription) {
  $form = array();
  $options = array();
  $default_value = array();
  global $language;

  // Get taxonomy terms for subscription form checkboxes.
  // With taxonomy translation and 'Per language terms' only the terms of the
  // current language are listed. With taxonomy translation and 'Localize terms'
  // all taxonomy terms are listed and translated.
  if (module_exists('i18ntaxonomy') && i18ntaxonomy_vocabulary(variable_get('simplenews_vid', '')) == I18N_TAXONOMY_TRANSLATE) {
    // Per language terms.
    $tterms = i18ntaxonomy_vocabulary_get_terms(variable_get('simplenews_vid', ''), $language->language);
    foreach ($tterms as $tid => $name) {
      $options[$tid] = check_plain($name);
      $default_value[$tid] = FALSE;
    }
  }
  else {
    foreach (taxonomy_get_tree(variable_get('simplenews_vid', '')) as $newsletter) {
      if (module_exists('i18ntaxonomy') && i18ntaxonomy_vocabulary(variable_get('simplenews_vid', '')) == I18N_TAXONOMY_LOCALIZE) {
        // Localize terms.
        $options[$newsletter->tid] = check_plain(tt('taxonomy:term:'. $newsletter->tid .':name', $newsletter->name, $language->language));;
      }
      else {
        // Untranslated.
        $options[$newsletter->tid] = check_plain($newsletter->name);
      }
      $default_value[$newsletter->tid] = FALSE;
    }
  }
  
  // Get subscriptions local user can see.
  global $user;
  $my_newsletters = array();
  $result = db_query("SELECT tid FROM {simplenews_sub_manager} WHERE uid = %d", $user->uid);
  while ($item = db_fetch_object($result)) {
    $my_newsletters[$item->tid] = NULL;
  }
  $options = array_intersect_key($options, $my_newsletters);

  $form['subscriptions'] = array(
    '#type' => 'fieldset',
    '#description' => t('Select the newsletter(s) to which you want to subscribe or unsubscribe.'),
  );
  $form['subscriptions']['newsletters'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => array_merge($default_value, (array)$subscription->tids),
  );

  // If current user is logged in, just display email.
  // Anonymous users see an email box and will receive confirmations
  if ($subscription->mail) {
    $form['subscriptions']['#title'] = t('Subscriptions for %mail', array('%mail' => $subscription->mail));
    $form['subscriptions']['mail'] = array('#type' => 'value', '#value' => $subscription->mail);
    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#weight' => 20,
    );
  }
  else {
    $form['subscriptions']['#title'] = t('Manage your newsletter subscriptions');
    $form['subscriptions']['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('email'),
      '#size' => 20,
      '#maxlength' => 128,
      '#weight' => 10,
      '#required' => TRUE,
    );
    $form['subscribe'] = array(
      '#type' => 'submit',
      '#value' => t('Subscribe'),
      '#weight' => 20,
    );
    $form['unsubscribe'] = array(
      '#type' => 'submit',
      '#value' => t('Unsubscribe'),
      '#weight' => 30,
    );
  }
  return $form;
}

/**
 * Menu callback: Mass subscribe to newsletters.
 *
 * @see simplenews_subscription_list_remove_submit()
 */
function simplenews_sub_manager_remove() {
  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline. Email addresses which are no longer subscribed to any newsletter, will be removed from the database.'),
  );
  $newsletters = array();
  foreach (taxonomy_get_tree(variable_get('simplenews_vid', '')) as $newsletter) {
    $newsletters[$newsletter->tid] = check_plain($newsletter->name);
  }

  global $user;
  $my_newsletters = array();
  $result = db_query("SELECT tid FROM {simplenews_sub_manager} WHERE uid = %d", $user->uid);
  while ($item = db_fetch_object($result)) {
    $my_newsletters[$item->tid] = NULL;
  }
  
  $form['newsletters'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Unsubscribe from'),
    '#options' => array_intersect_key($newsletters, $my_newsletters),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
  );
  return $form;
}

function simplenews_sub_manager_remove_submit($form, &$form_state) {
  $tree = taxonomy_get_tree(variable_get('simplenews_vid', ''));
  $removed = array();
  $invalid = array();
  $checked_newsletters = array_filter($form_state['values']['newsletters']);

  $emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  foreach ($emails as $email) {
    $email = trim($email);
    if (valid_email_address($email)) {
      foreach ($checked_newsletters as $tid) {
        $newsletter = taxonomy_get_term($tid);
        simplenews_unsubscribe_user($email, $newsletter->tid, FALSE);
        $removed[] = $email;
      }
    }
    else {
      $invalid[] = $email;
    }
  }
  if ($removed) {
    $removed = implode(", ", $removed);
    drupal_set_message(t('The following addresses were unsubscribed or removed: %removed.', array('%removed' => $removed)));

    $newsletter_names = array();
    foreach ($checked_newsletters as $tid) {
      $newsletter = taxonomy_get_term($tid);
      $newsletter_names[] = $newsletter->name;
    }
    drupal_set_message(t('The addresses were unsubscribed from the following newsletters: %newsletters.', array('%newsletters' => implode(', ', $newsletter_names))));
  }
  else {
    drupal_set_message(t('No addresses were removed.'));
  }
  if ($invalid) {
    $invalid = implode(", ", $invalid);
    drupal_set_message(t('The following addresses were invalid: %invalid.', array('%invalid' => $invalid)), 'error');
  }
}